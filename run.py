import os
from flask import Flask,request,jsonify, abort, make_response,url_for, g
from flask.ext.restful import Api, Resource, reqparse, fields, marshal
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.httpauth import HTTPBasicAuth
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer
                                               as Serializer, BadSignature, SignatureExpired)
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound

from app import app
from app import db

from app import User_handler
from app import Customer_handler
from app import ServiceProvider_handler
from app import Task_handler
from app import Review_handler
from app import misc

if __name__ == '__main__':
	#create database
	db.create_all()

	#Setting up url routes
	User_handler.setup_routes()
	Customer_handler.setup_routes()
	ServiceProvider_handler.setup_routes()
	Task_handler.setup_routes()
	Review_handler.setup_routes()
	misc.setup_routes()
	
	#start running app
	app.run(debug = True)