from flask.ext.restful import fields
from flask.ext.restful import Resource
from flask import jsonify, make_response
from app import auth
from app import api
from app.database import User
from flask.ext.restful import reqparse
from sqlalchemy.exc import IntegrityError
from app import db
from flask import g

#Fields for sending data to the front-end
user_fields = {
	'username' : fields.String,
	'Type' : fields.String,
	'pwd_hash' : fields.String,
	'uri': fields.Url('users')
}

@auth.verify_password
def verify_password(username_or_token,password):
	user = User.verify_auth_token(username_or_token)
	if not user:
		user = User.query.filter_by(username=username_or_token).first()
		if not user or not user.verify_pwd(password):
			return False
	g.user = user
	return True
    
class user(Resource):
	def __init__(self):
		#Parse input data from front-end
		self.reqparser = reqparse.RequestParser()
		self.reqparser.add_argument('username',type = str,location = 'json', required = True)
		self.reqparser.add_argument('password',type = str,location = 'json', required = True)
		self.reqparser.add_argument('type',type = str,location = 'json', required = True)
		super(user,self).__init__()
	def post(self):
		args = self.reqparser.parse_args()
		for a,b in args.iteritems():
			if a == 'username':
				user = User(username=b)
			if a == 'password':
				user.hash_pwd(b)
			if a == 'type':
				user.type = b
		db.session.add(user)
             
		try:
			db.session.commit()
			return make_response(jsonify({'Sucess':'Username successfully added'}))
		except IntegrityError:
			return make_response(jsonify({'Error':'Username exists already'}),500)
 
def setup_routes():
	api.add_resource(user,'/SmartServicesApp/api/v1.0/users',endpoint='users')    
	api.add_resource(user,'/SmartServicesApp/api/v1.0/users/<int:user_id>',endpoint='user')  