from flask import Flask
from flask.ext.restful import Api
from flask.ext.httpauth import HTTPBasicAuth
from flask.ext.sqlalchemy import SQLAlchemy

#Initialize app, api, auth and db
app = Flask(__name__, static_url_path="")
app.config['SECRET_KEY'] = 'dhjsak$RDFG@#$1dhJsdfklsdjfldjfsdf334233'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
api = Api(app)
auth = HTTPBasicAuth()
db = SQLAlchemy(app)


