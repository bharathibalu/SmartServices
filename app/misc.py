from flask.ext.restful import fields
from flask.ext.restful import Resource
from app import auth
from app import api
from . import app
from flask import g

#Fields for sending data to the front-end
area_fields = {
	'area' : fields.String
}

service_fields ={
	'service' : fields.String
}

serviceProviderInTask_fields={
	'reviewed':fields.String,
	'serviceProvider_id' :fields.Integer
}

class Token(Resource):
	decorators = [auth.login_required]
	def get(self):
		token = g.user.generate_auth_token(3000)
		return {'token': token.decode('ascii'), 'exp': 3000}
      
@app.route('/')
def root():
    return app.send_static_file('index.html')
  
  
def Unauthorized():
	return make_response(jsonify({'Error': 'Unauthorized'}),403)

def setup_routes():
	api.add_resource(Token,'/SmartServicesApp/api/v1.0/token',endpoint='token')  