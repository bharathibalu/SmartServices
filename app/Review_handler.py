from flask.ext.restful import Resource, fields, reqparse
from sqlalchemy.orm.exc import NoResultFound
from app import auth
from app import api
from app.database import ServiceProviderClass
from app.database import ReviewsClass
from flask import g
from app import db

#Fields for sending data to the front-end
review_fields = {
	'serviceProvider_id':fields.Integer,
	'customer_user_id' : fields.Integer,
	'rating' : fields.Integer, 
	'review' : fields.String, 
	'anonimity' : fields.String
}

class review(Resource):
	decorators = [auth.login_required]
	def __init__(self):
		#Parse input data from front-end
		self.reqparser = reqparse.RequestParser()
		self.reqparser.add_argument('serviceProvider_phone',type = int,location = 'json')
		self.reqparser.add_argument('rating',type = int,location = 'json')
		self.reqparser.add_argument('review',type = unicode,location = 'json')
		self.reqparser.add_argument('anonimity',type = unicode,location = 'json')
		super(review,self).__init__()
		
	def post(self):
		args = self.reqparser.parse_args()  
		received_data = dict([(key,value) for key,value in args.iteritems() if value!= None])
		try:
			serviceProvider = ServiceProviderClass.query.filter_by(phone=received_data['serviceProvider_phone']).first()
		except NoResultFound:
			abort(404)
		reviewObj = ReviewsClass(serviceProvider_id = serviceProvider.serviceProvider_id,customer_user_id = g.user.user_id,rating = received_data['rating'],review = received_data['review'],anonimity =received_data['anonimity'])
		db.session.add(reviewObj)
		db.session.commit()
		
def setup_routes():
	api.add_resource(review,'/SmartServicesApp/api/v1.0/review',endpoint='review')  