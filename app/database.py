from . import app
from app import db
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer
                                               as Serializer, BadSignature, SignatureExpired)
#Table definitions
class User(db.Model):
	__tablename__ = 'Users'
	user_id = db.Column(db.Integer,primary_key=True)
	username = db.Column(db.String(32),unique=True)
	pwd_hash = db.Column(db.String(64))
	type = db.Column(db.String(1),nullable=False)
	customers = db.relationship('CustomerClass', uselist=False, backref='user')
	serviceProviders = db.relationship('ServiceProviderClass', uselist=False, backref='user')
	AreasforUser = db.relationship("AreasForUserClass", backref="user",lazy='dynamic')
	ServicesbyserviceProvider = db.relationship("ServicesByServiceProviderClass", backref="user",lazy='dynamic')
	ReviewByUser = db.relationship("ReviewsClass", backref="user",lazy='dynamic')

	def hash_pwd(self,password):
		self.pwd_hash = pwd_context.encrypt(password)
             
	def verify_pwd(self, password):
		return pwd_context.verify(password, self.pwd_hash)
     
	def generate_auth_token(self,exp_time=600):
		s = Serializer(app.config['SECRET_KEY'], exp_time)
		return s.dumps({'user_id': self.user_id})
     
	@staticmethod        
	def verify_auth_token(token):
		s = Serializer(app.config['SECRET_KEY'])
		try:
			data = s.loads(token)
		except BadSignature:
			return None
		except SignatureExpired:
			return None
		user = User.query.get(data['user_id'])
		return user
 

class CustomerClass(db.Model):
	__tablename__ = 'customers'
	customer_id = db.Column(db.Integer,primary_key=True)
	user_id = db.Column(db.Integer,db.ForeignKey("Users.user_id"),unique=True)
	task = db.relationship('TaskClass', backref='customer',lazy='dynamic')
	name = db.Column(db.String(32), nullable=False)
	gender = db.Column(db.String(1),nullable=False)
	phone = db.Column(db.Integer,nullable=False) 
	alternative_phone = db.Column(db.Integer) 
	email = db.Column(db.String(32),nullable=False) 
       
class ServiceProviderClass(db.Model):
	__tablename__ = 'serviceProviders'
	serviceProvider_id = db.Column(db.Integer,primary_key=True)
	user_id = db.Column(db.Integer,db.ForeignKey("Users.user_id"),unique=True)
	ServiceProviderInTask = db.relationship('ServiceProviderInTaskClass', backref='serviceProvider',
									lazy='dynamic')
	name = db.Column(db.String(32), nullable=False)
	gender = db.Column(db.String(1), nullable=False)
	company_type = db.Column(db.String(1), nullable=False)
	phone = db.Column(db.Integer, nullable=False) 
	alternative_phone = db.Column(db.Integer) 
	email = db.Column(db.String(32), nullable=False) 
	rate_per_hr = db.Column(db.Integer, nullable=False)
	evening_work = db.Column(db.String(1), nullable=False) #will you work in evenings? y or n?
	weekend_work = db.Column(db.String(1), nullable=False) #will you work in weekends? y or n?
	availability = db.Column(db.String(1)) #y or n. Available today or not?
	reviews = db.relationship('ReviewsClass', backref='serviceProvider',
									lazy='dynamic')
     
class TaskClass(db.Model):
	__tablename__ = 'tasks'
	task_id = db.Column(db.Integer,primary_key=True)
	customer_id = db.Column(db.Integer,db.ForeignKey("customers.customer_id"))
	serviceProvider = db.relationship("ServiceProviderInTaskClass", backref="task",lazy='dynamic')
	area = db.Column(db.String(32),nullable=False)
	type = db.Column(db.String(1),nullable=False)  #one time or monthly
	service = db.Column(db.String(32),nullable=False)
	evening_only = db.Column(db.String(1))
	weekend_only = db.Column(db.String(1))
	starting_price = db.Column(db.Integer)
	ending_price = db.Column(db.Integer)
	
class AreasClass(db.Model):
	__tablename__='areas'
	area_id = db.Column(db.Integer,primary_key=True)
	area = db.Column(db.String(32),unique=True)
	user = db.relationship("AreasForUserClass", backref="area",lazy='dynamic')
	   
class AreasForUserClass(db.Model):
	__tablename__='AreasForUser'
	id = db.Column(db.Integer,primary_key=True)
	user_id = db.Column(db.Integer,db.ForeignKey("Users.user_id"))
	area_id = db.Column(db.Integer,db.ForeignKey("areas.area_id"))
	#UniqueConstraint('user_id', 'area_id')
	#Combined unique constraint with user_id and area_id is needed
	
class ServicesClass(db.Model):
	__tablename__='services'
	service_id = db.Column(db.Integer,primary_key=True)
	service = db.Column(db.String(32))
	user = db.relationship("ServicesByServiceProviderClass", backref="area",lazy='dynamic')
	   
class ServicesByServiceProviderClass(db.Model):
	__tablename__='ServicesByserviceProvider'
	id = db.Column(db.Integer,primary_key=True)
	user_id = db.Column(db.Integer,db.ForeignKey("Users.user_id"))
	service_id = db.Column(db.Integer,db.ForeignKey("services.service_id"))

class ReviewsClass(db.Model):
	__tablename__='reviews'
	id = db.Column(db.Integer,primary_key=True)
	serviceProvider_id = db.Column(db.Integer,db.ForeignKey("serviceProviders.serviceProvider_id"))
	customer_user_id = db.Column(db.Integer,db.ForeignKey("Users.user_id"))
	rating = db.Column(db.Integer, nullable = False)
	review = db.Column(db.String(500))
	anonimity = db.Column(db.String(1), nullable = False)
	
class ServiceProviderInTaskClass(db.Model):
	__tablename__ = 'ServiceProviderInTask'
	id = db.Column(db.Integer,primary_key=True)
	task_id = db.Column(db.Integer,db.ForeignKey("tasks.task_id"))
	serviceProvider_id = db.Column(db.Integer,db.ForeignKey("serviceProviders.serviceProvider_id"))
	reviewed = db.Column(db.String(1))