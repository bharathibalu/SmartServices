from flask.ext.restful import Resource, marshal, fields, reqparse
from sqlalchemy.orm.exc import NoResultFound
from app import auth
from app import api
from app.database import ServiceProviderClass
from app.database import ServicesClass
from app.database import ServicesByServiceProviderClass
from app.database import AreasClass
from app.database import AreasForUserClass
from flask import g
from app import db
from misc import area_fields
from misc import service_fields

#Fields for sending data to the front-end
serviceProvider_fields = {
	'user_id':fields.Integer,
	'serviceProvider_id':fields.Integer,
	'name' : fields.String,
	'gender' : fields.String,
	'company_type' : fields.String, #Office or Individual
	'phone' : fields.Integer, 
	'alternative_phone' : fields.Integer, 
	'email' : fields.String, 
	'rate_per_hr' : fields.Integer,
	'evening_work' : fields.String, #will you work in evenings? y or n?
	'weekend_work' : fields.String, #will you work in weekends? y or n?
	'Availability' : fields.String #y or n. Available today or not?
}
  
class serviceProvider(Resource):
	decorators = [auth.login_required]
	def __init__(self):
		#Parse input data from front-end
		self.reqparser = reqparse.RequestParser()
		self.reqparser.add_argument('name',type = unicode,location = 'json')
		self.reqparser.add_argument('gender',type = unicode,location = 'json')
		self.reqparser.add_argument('company_type',type = unicode,location = 'json')
		self.reqparser.add_argument('area',type = list,location = 'json')
		self.reqparser.add_argument('phone',type = int,location = 'json')
		self.reqparser.add_argument('alternative_phone',type = int,location = 'json')
		self.reqparser.add_argument('email',type = unicode,location = 'json')
		self.reqparser.add_argument('service',type = list,location = 'json')
		self.reqparser.add_argument('rate_per_hr',type = int,location = 'json')
		self.reqparser.add_argument('evening_work',type = unicode,location = 'json')
		self.reqparser.add_argument('weekend_work',type = unicode,location = 'json')
                                         
		super(serviceProvider,self).__init__()
           
	def get(self):
		try:
			serviceProvider = ServiceProviderClass.query.filter_by(user_id=g.user.user_id).first()
			user_areas = AreasForUserClass.query.filter_by(user_id=g.user.user_id).all()
			serviceProvider_services = ServicesByServiceProviderClass.query.filter_by(user_id=g.user.user_id).all()
			serviceProvider_areas = [AreasClass.query.filter_by(area_id=user_area.area_id).first() for user_area in user_areas]
			serviceProvider_services = [ServicesClass.query.filter_by(service_id=serviceProvider_service.service_id).first() for serviceProvider_service in serviceProvider_services]
			serviceProviderAreaList = []
			serviceProviderServiceList = []
			for serviceProvider_area in serviceProvider_areas:
				serviceProviderAreaList.append(serviceProvider_area.area)
			for serviceProvider_service in serviceProvider_services:	
				serviceProviderServiceList.append(serviceProvider_service.service)
              
		except NoResultFound:
			abort(404)
		return {'ServiceProvider': marshal(serviceProvider,serviceProvider_fields), 'ServiceProvider_areas':serviceProviderAreaList, 'ServiceProvider_services':serviceProviderServiceList}
 
	def put(self):    
		try:
			Updated_ServiceProvider = ServiceProviderClass.query.filter_by(user_id=g.user.user_id).first()
		except NoResultFound:
			abort(404)
             
		args = self.reqparser.parse_args()  
		received_data = dict([(key,value) for key,value in args.iteritems() if value!= None])
       
		Updated_ServiceProvider.user_id = g.user.user_id                
		Updated_ServiceProvider.name = received_data['name']
		Updated_ServiceProvider.gender = received_data['gender']
		Updated_ServiceProvider.company_type = received_data['company_type']
		Updated_ServiceProvider.phone = received_data['phone']
		Updated_ServiceProvider.alternative_phone = received_data['alternative_phone']
		Updated_ServiceProvider.email = received_data['email']
		Updated_ServiceProvider.rate_per_hr = received_data['rate_per_hr']
		Updated_ServiceProvider.evening_work = received_data['evening_work']
		Updated_ServiceProvider.weekend_work = received_data['weekend_work']
       
				
		db.session.query(AreasForUserClass).filter(AreasForUserClass.user_id==g.user.user_id).delete()
		db.session.query(ServicesByServiceProviderClass).filter(ServicesByServiceProviderClass.user_id==g.user.user_id).delete()
		db.session.commit()
		
		areasObjList = []
		for area in received_data['area']:
			areasObj = AreasClass.query.filter_by(area=area).first()
			if areasObj == None:
				areasObj = AreasClass(area = area)
				db.session.add(areasObj)
				db.session.flush()
                       
			areasObjList.append(areasObj)
			AreasForUserObj = AreasForUserClass(area_id = areasObj.area_id, user_id = g.user.user_id)
			db.session.add(AreasForUserObj)
			
		servicesObjList = []
		for service in received_data['service']:
			servicesObj = ServicesClass.query.filter_by(service=service).first()
			if servicesObj == None:
				servicesObj = ServicesClass(service = service)
				db.session.add(servicesObj)
				db.session.flush()
                       
			servicesObjList.append(servicesObj)
			ServicesByServiceProviderObj = ServicesByServiceProviderClass(service_id = servicesObj.service_id, user_id = g.user.user_id)
			db.session.add(ServicesByServiceProviderObj)	
		
		db.session.commit()
                                       
		New_ServiceProvider = ServiceProviderClass.query.all()
		return {"Updated_ServiceProvider":marshal(Updated_ServiceProvider,serviceProvider_fields),'Updated_areas':map(lambda x: marshal(x,area_fields),areasObjList), 'Updated_services':map(lambda x: marshal(x,service_fields),servicesObjList)}
			   
	def post(self):  
		args = self.reqparser.parse_args()               
		serviceProviderObj = ServiceProviderClass(user_id = g.user.user_id, name = args['name'], gender = args['gender'], company_type = args['company_type'], phone = args['phone'], alternative_phone = args['alternative_phone'], email = args['email'], rate_per_hr = args['rate_per_hr'], evening_work = args['evening_work'], weekend_work = args['weekend_work'])
		db.session.add(serviceProviderObj)
               
		areasObjList = []
		for area in args['area']:
			areasObj = AreasClass.query.filter_by(area=area).first()
			if areasObj == None:
				areasObj = AreasClass(area = area)
				db.session.add(areasObj)
				db.session.flush()
                     
			areasObjList.append(areasObj)
			AreasForUserObj = AreasForUserClass(area_id = areasObj.area_id, user_id = g.user.user_id)
			db.session.add(AreasForUserObj)
			
		servicesObjList = []
		for service in args['service']:
			servicesObj = ServicesClass.query.filter_by(service=service).first()
			if servicesObj == None:
				servicesObj = ServicesClass(service = service)
				db.session.add(servicesObj)
				db.session.flush()
                    
			servicesObjList.append(servicesObj)
			ServicesByServiceProviderObj = ServicesByServiceProviderClass(service_id = servicesObj.service_id, user_id = g.user.user_id)
			db.session.add(ServicesByServiceProviderObj)	
			
		db.session.commit()        
               
		return {'Newly_added_ServiceProvider' : marshal(serviceProviderObj, serviceProvider_fields),'Newly_added_areas':map(lambda x: marshal(x,area_fields),areasObjList),'Newly_added_services':map(lambda x: marshal(x,service_fields),servicesObjList)}
               			
def setup_routes():
	api.add_resource(serviceProvider,'/SmartServicesApp/api/v1.0/serviceProvider',endpoint='serviceProvider')  			