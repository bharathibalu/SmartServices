Welcome to your tutorial about SmartServices!
====================
This is an app where customers can post a task and find suitable service Providers.

Features
=========
1. This is simple click based intuitive mobile app. Web-based app is also available.
2. Tasks can be anything from a one-time job like painting, carpentering to monthly-salaried job like maid, nanny, etc.
3. Based on the requirements quoted in the task, suitable service providers who are currently available(to take calls) are listed.
4. Along with the other service provider details, any reviews available for the service provider is also provided.
5. Customers contact details are never shared with the service providers for privacy purposes.
6. Seemless app for end-to-end solution;from posting tasks to paying/providing reviews to the service provider.

Vola, you are ready to start now!!! Go have fun :)